# [1.2.0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/php-client/compare/v1.1.2...v1.2.0) (2022-05-04)


### Features

* timeout and connectTimeout options ([5ebaaa8](https://gitlab.ics.muni.cz/perun/perun-proxyidp/php-client/commit/5ebaaa8e58bae77f9b769821b487ec3f5312e22f))

## [1.1.2](https://gitlab.ics.muni.cz/perun/perun-proxyidp/php-client/compare/v1.1.1...v1.1.2) (2022-05-02)


### Bug Fixes

* handle empty auth result as null ([df69edb](https://gitlab.ics.muni.cz/perun/perun-proxyidp/php-client/commit/df69edbb5a7603cdabcccb76477d54cdacf324b7))

## [1.1.1](https://gitlab.ics.muni.cz/perun/perun-proxyidp/php-client/compare/v1.1.0...v1.1.1) (2022-05-02)


### Bug Fixes

* handle empty auth result as null ([56cbd76](https://gitlab.ics.muni.cz/perun/perun-proxyidp/php-client/commit/56cbd768f41f37148d24613508b0a84c2f1ee7e5))

# [1.1.0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/php-client/compare/v1.0.0...v1.1.0) (2022-04-27)


### Features

* process result->authentication and add to the PIResponse ([3dfadc8](https://gitlab.ics.muni.cz/perun/perun-proxyidp/php-client/commit/3dfadc88915fe073da5d8da8391af326736e7059))

# [1.0.0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/php-client/compare/v0.9.7...v1.0.0) (2022-03-21)


### Bug Fixes

* namespace for autoload ([9d45557](https://gitlab.ics.muni.cz/perun/perun-proxyidp/php-client/commit/9d45557fd890e9b7188636a57da0a746418129b7))


### Features

* edit composer.json and README for this fork ([18a0113](https://gitlab.ics.muni.cz/perun/perun-proxyidp/php-client/commit/18a011318570d8bdf3a0d0cda8ee1d4e9d8a58e9))
* use namespaces, finally ([ee4c8de](https://gitlab.ics.muni.cz/perun/perun-proxyidp/php-client/commit/ee4c8de2f977c648a5150888e71b20c2125c4fb0))


### BREAKING CHANGES

* classes use namespaces, removed autoloader
* new name for the composer package
