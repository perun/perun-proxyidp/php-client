<?php

declare(strict_types=1);

namespace PrivacyIDEA\PHPClient;

abstract class AuthenticationStatus
{
    public const CHALLENGE = 'CHALLENGE';

    public const ACCEPT = 'ACCEPT';

    public const REJECT = 'REJECT';

    public const NONE = 'NONE';
}
