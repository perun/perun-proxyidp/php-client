<?php

declare(strict_types=1);

namespace PrivacyIDEA\PHPClient;

class PIChallenge
{
    /**
     * @var string type of the token this challenge is for
     */
    public $type = '';

    /**
     * @var string message for this challenge
     */
    public $message = '';

    /**
     * @var string transactionId to reference this challenge in later requests
     */
    public $transactionID = '';

    /**
     * @var string serial of the token this challenge is for
     */
    public $serial = '';

    /**
     * @var string arbitrary attributes that can be appended to the challenge by the server
     */
    public $attributes;

    /**
     * @var string JSON format
     */
    public $webAuthnSignRequest = '';

    /**
     * @var string JSON format
     */
    public $u2fSignRequest = '';
}
